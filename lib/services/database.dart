import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:Seeker/models/user.dart';

class DatabaseService{

  final String uid;
  DatabaseService({ this.uid});

  final CollectionReference userData = Firestore.instance.collection("userData");

  Future updateUserData(String name, String surname, int height) async {
    return await userData.document(uid).setData({
      'name' : name,
      'surname' : surname,
      'height' : height,
    });
  }

  //iz baze snapshota podatke userja
  UserData _userDataFromSnapshots(DocumentSnapshot snapshot){
    return UserData(
      uid: uid,
      name: snapshot.data['name'],
      surname: snapshot.data['surname'],
      height: snapshot.data['height'],
    );
  }

  Stream<UserData> get userDataStream {
    //print(uid);
    return userData.document(uid).snapshots().map(_userDataFromSnapshots);
  }

}