import 'package:firebase_auth/firebase_auth.dart';
import 'package:Seeker/models/user.dart';
import 'package:Seeker/services/database.dart';

class AuthService {
  // _ = private variable
  final FirebaseAuth _auth = FirebaseAuth.instance;

  //create user obj based on Firebaseuser data
  //extractamo uid iz vseh podatkov
  User _userfromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uid: user.uid) : null;
  }

  //returna  user ko zazna change login, signout
  Stream<User> get user {
    //convertamo firebase user v nas user
    return _auth.onAuthStateChanged.map(_userfromFirebaseUser);
  }

  // sign in anonymously
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userfromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // sign in with email and password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      if (user.isEmailVerified) return _userfromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // register with email and password
  Future registerWithEmailAndPassword(String ime, String priimek, int visina,
      String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      try {
        await user.sendEmailVerification();
        print(user.uid + "vspešno poslana verifikacija");
      } catch (e) {
        print("napaka pri email verifikaciji");
        print(e.message);
      }
      //kreiranje novega dokumenta za novega uporabnika prek njegovega uid
      await DatabaseService(uid: user.uid).updateUserData(ime, priimek, visina);

      return _userfromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<void> resetPassword(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

  Future newEmail(String email) async {
    try {
    FirebaseUser user = await _auth.currentUser();
    user.updateEmail(email);
      return "succes";
    } catch (e) {
      print(e.toString());
      return "error";
    }
  }
}