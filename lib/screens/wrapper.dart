import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:Seeker/screens/authenticate/begin.dart';
import 'package:Seeker/screens/home/home.dart';
import 'package:Seeker/models/user.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //nastavi value vsakic ko dobi new value
    final user = Provider.of<User>(context);
    // return either the Home or Authenticate widget
    if (user == null) {
      return Begin();
    } else {
      return Home();
    }
  }
}