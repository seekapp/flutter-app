import 'package:flutter/material.dart';
import 'package:Seeker/screens/settings/change_height.dart';
import 'package:Seeker/screens/settings/change_email.dart';
import 'package:Seeker/screens/settings/change_password.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        	body: Column(
            children: <Widget>[
              Center(child: SizedBox(height: 50),),
              Text(
                "Nastavitve",
                style: TextStyle(fontSize: 30),
              ),
              SizedBox(height: 70),
              Text("Uporabniške nastavitve"),
              SizedBox(height: 30),
              ButtonTheme(
              minWidth: 200,
              height: 50,
              child: RaisedButton(
                child: Text(
                  'Spremeni višino',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                onPressed: _sendToChangeHeight,
              ),
            ),
            SizedBox(height: 10),
            ButtonTheme(
              minWidth: 200,
              height: 50,
              child: RaisedButton(
                child: Text(
                  'Spremeni geslo',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                onPressed: _sendToChangePassword,
              ),
            ),
            SizedBox(height: 10),
            ButtonTheme(
              minWidth: 200,
              height: 50,
              child: RaisedButton(
                child: Text(
                  'Spremeni email',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                onPressed: _sendToChangeEmail,
              ),
            ),
            SizedBox(height: 30),
            Text("Ostale nastavitve"),
            ],
          )
      ,)
    );
  }

  _sendToChangePassword() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChangePassword()),
    );
  } 

  _sendToChangeHeight() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChangeHeight()),
    );
  }

  _sendToChangeEmail(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChangeEmail()),
    );
  }
}