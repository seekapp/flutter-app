import 'package:flutter/material.dart';
import 'package:Seeker/services/auth.dart';

class ChangePassword extends StatefulWidget {
  ChangePassword({Key key}) : super(key: key);
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  GlobalKey<FormState> _key = new GlobalKey(); // key ki tracka form state
  final AuthService _auth = AuthService();
  String _noviPassword;
  String error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(20.0),
            child: Center(
              child: Form(
                key: _key,
                autovalidate: true,
                child: Column(
                  children: <Widget>[
                    new Image.asset(
                      'assets/logo.png', // asset folder je declaran v pubspec.yaml
                      height: 150,
                      width: 300,
                    ),
                    new SizedBox(height: 50.0), // empty box(space)
                    new TextFormField(
                        autofocus: false,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          hintText: 'vaš email',
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                        ),
                        validator: (value) {
                          String pattern =
                              r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
                          RegExp regExp = new RegExp(pattern);
                          if (value.isEmpty) {
                            return "Vnesite email naslov!";
                          } else if (!regExp.hasMatch(value)) {
                            return "Neveljaven email naslov!";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (String value) {
                          _noviPassword =
                              value; // on saved se zgodi ko klicemo _formKey.currentState.save()
                        }),
                    new SizedBox(height: 15.0),
                    new Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(24),
                        ),
                        onPressed: _sendToServer,
                        padding: EdgeInsets.all(12),
                        color: Colors.blue,
                        child: Text('Spremeni',
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                    new Text(
                      error,
                      style: TextStyle(color: Colors.red, fontSize: 14.0),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _sendToServer() async {
    if (_key.currentState.validate()) {
      _key.currentState.save(); // sprozi onSaved pri email pa passwordu
      print("Novo geslo $_noviPassword");
      await _auth.resetPassword(_noviPassword);
    }
  }
}
