import 'package:provider/provider.dart';
import 'package:Seeker/models/user.dart';
import 'package:Seeker/screens/home/bluetooth.dart';
import 'package:Seeker/screens/home/userprofile.dart';
import 'package:Seeker/screens/settings/settings.dart';
import 'package:Seeker/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:Seeker/services/database.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:encrypt/encrypt.dart' as enc;
import 'package:intl/intl.dart';
import 'package:battery/battery.dart';
import 'dart:async';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var battery = Battery();
  var btConectionTimer = Stopwatch();
  int _baterija;
  String _razdalja = "0.0";
  int _stkorakov = 0;
  String _time;
  String _date;
  String _btTimer = "00:00:00";
  final AuthService _auth = AuthService();
  enc.Encrypted tempenc;
  final myController = TextEditingController();

  @override
  void initState() {
    _time = _formatTime(DateTime.now());
    _date = _formatDate(DateTime.now());
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    /*
    String besedilo = "Lorem ipsum";
    enc.Encrypted zakodirano = encrypt(besedilo);
    print("Zakodirano = " + zakodirano.base64);
    print("Odkodirano = " + decrypt(zakodirano));
    */

    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user.uid).userDataStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserData userData = snapshot.data;
            return Container(
              child: Scaffold(
                backgroundColor: Colors.white,
                drawer: Drawer(
                  child: ListView(
                    children: <Widget>[
                      DrawerHeader(
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Text("SEEKER MENU"),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                              child: Text("IME:  " + userData.name),
                            ),
                            Container(
                              child: Text("PRIIMIKE:  " + userData.surname),
                            ),
                            Container(
                              child: Text(
                                  "VIŠINA:  " + userData.height.toString()),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.blue,
                        ),
                      ),
                      ListTile(
                          title: Text('Bluetooth '), onTap: _sendToBluetooth),
                      ListTile(
                          title: Text('Moj profil'), onTap: _sendToUserProfile),
                      ListTile(
                          title: Text('Nastavitve'), onTap: _sendToSettings),
                      ListTile(
                        title: Text('Odjavi se'),
                        onTap: () async {
                          await _auth.signOut();
                        },
                      ),
                      ListTile(
                        title: Text('Testiranje padca očal'),
                        onTap: _showDialog,
                      ),
                      ListTile(
                        title: Text('Testiranje enkripcije/dekripcije'),
                        onTap: _showEnc,
                      ),
                      ListTile(
                        title: Text('Find my glass'),
                        onTap: _showDialogFindMyGlass, // dodati je potrebno funkcijo za pošiljanje signala na očala
                      ),
                      ListTile(
                        title: Text('Začetek bluetooth povezave'),
                        onTap: _startBTconection,
                      ),
                      ListTile(
                        title: Text('Konec bluetooth povezave'),
                        onTap: _restartBTconection,
                      ),
                      ListTile(
                        title: Text('$_btTimer'),
                        leading: CircleAvatar(
                          backgroundImage: AssetImage('assets/bluetooth.png'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                      ListTile(
                        title: Text('$_stkorakov'),
                        leading: CircleAvatar(
                          backgroundImage: AssetImage('assets/shoe.png'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                      ListTile(
                        title: Text('$_razdalja km'),
                        leading: CircleAvatar(
                          backgroundImage: AssetImage('assets/distance.png'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                      ListTile(
                        title: Text('$_date'),
                        leading: CircleAvatar(
                          backgroundImage: AssetImage('assets/date.png'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                      ListTile(
                        title: Text('$_time'),
                        leading: CircleAvatar(
                          backgroundImage: AssetImage('assets/clock.png'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                      ListTile(
                        title: Text('$_baterija%'),
                        leading: CircleAvatar(
                          backgroundImage: AssetImage('assets/battery.png'),
                          backgroundColor: Colors.transparent,
                        ),
                      )
                    ],
                  ),
                ),
                body: Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Tabela Delovanja pasu",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 50),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              color: Colors.blue[50],
                              height: 50.0,
                              width: 50.0,
                            ),
                            Container(
                              color: Colors.blue[100],
                              height: 50.0,
                              width: 50.0,
                            ),
                            Container(
                              color: Colors.blue,
                              height: 50.0,
                              width: 50.0,
                            ),
                            Container(
                              color: Colors.blue[200],
                              height: 50.0,
                              width: 50.0,
                            ),
                            Container(
                              color: Colors.blue[300],
                              height: 50.0,
                              width: 50.0,
                            ),
                          ],
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                color: Colors.red[50],
                                height: 50.0,
                                width: 50.0,
                              ),
                              Container(
                                color: Colors.red[100],
                                height: 50.0,
                                width: 50.0,
                              ),
                              Container(
                                color: Colors.red,
                                height: 50.0,
                                width: 50.0,
                              ),
                              Container(
                                color: Colors.red[200],
                                height: 50.0,
                                width: 50.0,
                              ),
                              Container(
                                color: Colors.red[300],
                                height: 50.0,
                                width: 50.0,
                              ),
                            ])
                      ]),
                ),
              ),
            );
          } else {
            return Container(
              child: Scaffold(
                backgroundColor: Colors.white,
                drawer: Drawer(
                  child: ListView(
                    children: <Widget>[
                      DrawerHeader(
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Text("SEEKER MENU"),
                            ),
                            Container(
                              child: Text("NO DATA..."),
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.blue,
                        ),
                      ),
                      ListTile(
                          title: Text('Nastavitve'), onTap: _sendToSettings),
                      ListTile(
                        title: Text('Odjavi se'),
                        onTap: () async {
                          await _auth.signOut();
                        },
                      ),
                      FlatButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Bluetooth()),
                          );
                        },
                        child: Text("Bluetooth"),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
        });
  }

  void _getTime() async {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatTime(now);
    _baterija = await battery.batteryLevel;
    setState(() {
      _time = formattedDateTime;
      _razdalja = _izracunRazdalje(_stkorakov);
      _stkorakov++;
    });
  }

  String _izracunRazdalje(int stkorakov) {
    // 1 km = 1312 povprečnih korakov
    return (_stkorakov / 1312).toStringAsFixed(2);
  }

  String _formatTime(DateTime dateTime) {
    return DateFormat.Hm().format(dateTime);
  }

  String _formatDate(DateTime dateTime) {
    return DateFormat.yMMMd().format(dateTime);
  }

  _sendToSettings() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Settings()));
  }

  _sendToUserProfile() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => UserProfile()));
  }

  _sendToBluetooth() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Bluetooth()));
  }

  enc.Encrypted encrypt(String besedilo) {
    final key = enc.Key.fromUtf8("seekerseekerseekerseekerseekerse");
    final iv = enc.IV.fromLength(16);
    final encrypter = enc.Encrypter(enc.AES(key));
    final encrypted = encrypter.encrypt(besedilo, iv: iv);
    return encrypted;
  }

  String decrypt(enc.Encrypted zakodirano) {
    final key = enc.Key.fromUtf8("seekerseekerseekerseekerseekerse");
    final iv = enc.IV.fromLength(16);
    final encrypter = enc.Encrypter(enc.AES(key));
    final decrypted = encrypter.decrypt(zakodirano, iv: iv);
    return decrypted;
  }

  void _showDialog() {
    FlutterRingtonePlayer.play(
      android: AndroidSounds.alarm,
      ios: IosSounds.glass,
      looping: true,
      volume: 0.08,
      asAlarm: false,
    );
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Opozorilo"),
          content: new Text("Zaznan padec očal!"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Nazaj"),
              onPressed: () {
                FlutterRingtonePlayer.stop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
   void _showDialogFindMyGlass() {
    FlutterRingtonePlayer.play(
      android: AndroidSounds.alarm,
      ios: IosSounds.glass,
      looping: true,
      volume: 0.08,
      asAlarm: false,
    );
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Obvestilo"),
          content: new Text("Očala so bila najdena!"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Nazaj"),
              onPressed: () {
                FlutterRingtonePlayer.stop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _showEnc() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Enkripcija/dekripcija'),
            content: new TextFormField(
              controller: myController,
              //text box za email form uporabljamo za form validatorje
              keyboardType: TextInputType.text,
              autofocus: true,
              decoration: InputDecoration(
                hintText: 'Vnesi za enkripcijo/dekripcijo',
                contentPadding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 15.0),
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('Enkripcija'),
                onPressed: () {
                  enc.Encrypted coded;
                  coded = encrypt(myController.text);
                  myController.text = coded.base64;
                  tempenc = coded;
                },
              ),
              new FlatButton(
                child: new Text('Dekripcija'),
                onPressed: () {
                  String dec;
                  dec = decrypt(tempenc);
                  myController.text = dec;
                },
              ),
              new FlatButton(
                child: new Text('Izhod'),
                onPressed: () {
                  tempenc = null;
                  myController.text = "";
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  _testKomponent() {}

  _startBTconection() {
    btConectionTimer.start();
    starttimer();
  }

  _restartBTconection() {
    _btTimer = "00:00:00";
    btConectionTimer.reset();
    btConectionTimer.stop();
  }

  void starttimer() {
    Timer(Duration(seconds: 1), keeprunning);
  }

  void keeprunning() {
    if (btConectionTimer.isRunning) {
      starttimer();
    }
    setState(() {
      _btTimer = btConectionTimer.elapsed.inHours.toString().padLeft(2, "0") +
          ":" +
          (btConectionTimer.elapsed.inMinutes % 60).toString().padLeft(2, "0") +
          ":" +
          (btConectionTimer.elapsed.inSeconds % 60).toString().padLeft(2, "0");
    });
  }
}
