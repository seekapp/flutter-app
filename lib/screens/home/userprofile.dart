import 'package:provider/provider.dart';
import 'package:Seeker/models/user.dart';
import 'package:flutter/material.dart';
import 'package:Seeker/services/database.dart';

class UserProfile extends StatefulWidget {
  UserProfile({Key key}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  //final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user.uid).userDataStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserData userData = snapshot.data;
            return Scaffold(
              body: Column(
                children: [
                  SizedBox(height: 130),
                  Center(
                    child: Image.asset(
                      'assets/user.png',
                      height: 150,
                      width: 300,
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Prijavljeni ste kot: ' +
                        userData.name +
                        ' ' +
                        userData.surname,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Višina: ' + userData.height.toString() + 'cm',
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 70,
                  ),
                ],
              ),
              backgroundColor: Colors.white,
            );
          } else {
            return Container(width: 0.0, height: 0.0);
          }
        });
  }
}
