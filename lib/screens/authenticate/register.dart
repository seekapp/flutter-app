import 'package:flutter/material.dart';
import 'package:Seeker/screens/authenticate/sign_in.dart';
import 'package:Seeker/services/auth.dart';

class Register extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _RegisterState();
  }
}

class _RegisterState extends State<Register> {
  final AuthService _auth = AuthService();
  GlobalKey<FormState> _key = new GlobalKey(); // key ki tracka form state
  bool _validate = false; // bool ce hocemo da se autovalidata form
  RegisterRequestData _registerData =
      RegisterRequestData(); // class za variabli za login
  bool skrijGeslo = true; // bool da skrijemo password text

  //statefull cidget pri setState() refresha samo kar je v buildo(layout)
  //nebo naprimer na novo instanciralo variablov
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // basic element z katerim se vsak app zacne
      body: new Center(
        // v  body se dajaejo "widgeti", center centrira vertikalno
        child: new SingleChildScrollView(
          // child centra omogoča scrolanje
          child: new Container(
            // child centra, nested
            margin: new EdgeInsets.all(20.0),
            child: Center(
              child: new Form(
                key: _key,
                autovalidate: _validate,
                child: _getFormUI(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getFormUI() {
    return new Column(
      // returna column v katerega lahko damo vec elementov
      children: <Widget>[
        // razporejeni bojo v "rowe"
        new Image.asset(
          // nalozimo sliko iz folderja
          'assets/logo.png', // asset folder je declaran v pubspec.yaml
          height: 150,
          width: 300,
        ),
        new SizedBox(height: 25.0), // empty box(space)
        new Container(
          alignment: Alignment.centerLeft,
          child: Text("Ime: "),
        ),
        new SizedBox(
          height: 5.0,
        ),
        new TextFormField(
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            hintText: 'Ime',
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          validator: FormValidator().validateIme, //validator
          onSaved: (String value) {
            _registerData.ime = value;
          },
        ),
        new SizedBox(height: 5.0),
        new Container(
          alignment: Alignment.centerLeft,
          child: Text("Priimek: "),
        ),
        new SizedBox(
          height: 5.0,
        ),
        new TextFormField(
          // priimek
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
            hintText: 'Priimek',
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          validator: FormValidator().validatePriimek, //validator
          onSaved: (String value) {
            _registerData.priimek = value;
          },
        ),
        new SizedBox(height: 5.0),
        new Container(
          alignment: Alignment.centerLeft,
          child: Text("Višina: "),
        ),
        new SizedBox(
          height: 5.0,
        ),
        new TextFormField(
          // višina
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
            hintText: 'Višina',
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          validator: FormValidator().validateVisina, //validator
          onSaved: (String value) {
            _registerData.visina = int.parse(value);
          },
        ),
        new SizedBox(height: 5.0),
        new Container(
          alignment: Alignment.centerLeft,
          child: Text("Email: "),
        ),
        new SizedBox(
          height: 5.0,
        ),
        new TextFormField(
          //text box za email form uporabljamo za form validatorje
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
            hintText: 'Email',
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          validator: FormValidator().validateEmail, //validator
          onSaved: (String value) {
            _registerData.email = value;
          },
        ),
        new SizedBox(height: 5.0),
        new Container(
          alignment: Alignment.centerLeft,
          child: Text("Geslo: "),
        ),
        new SizedBox(
          height: 5.0,
        ),
        new TextFormField(
            autofocus: false,
            obscureText: skrijGeslo,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: 'Geslo',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
              suffixIcon: GestureDetector(
                //gesture detection kjer lahko detectamo tape
                onTap: () {
                  setState(() {
                    skrijGeslo = !skrijGeslo; //nastavi false na true in obratno
                  });
                },
                child: Icon(
                  skrijGeslo
                      ? Icons.visibility
                      : Icons.visibility_off, // statement ? true : false
                  semanticLabel: skrijGeslo ? 'prikaži geslo' : 'skrij geslo',
                ),
              ),
            ),
            validator: FormValidator().validatePassword,
            onSaved: (String value) {
              _registerData.password =
                  value; // on saved se zgodi ko klicemo _formKey.currentState.save()
            }),
        new SizedBox(height: 10.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 6.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: _sendToServer,
            padding: EdgeInsets.all(12),
            color: Colors.blue,
            child: Text('Registracija', style: TextStyle(color: Colors.white)),
          ),
        ),
        new FlatButton(
          onPressed: _sendToSignInPage,
          child: Text('Že imate račun? Prijava!',
              style: TextStyle(color: Colors.black54)),
        ),
      ],
    );
  }

  _sendToSignInPage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignIn()),
    );
  }
  /*
  _sendToHome() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Wrapper()),
    );
  }*/

  _sendToServer() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save(); // sprozi onSaved pri email pa passwordu
      print("Ime ${_registerData.ime}");
      print("Priimek ${_registerData.priimek}");
      print("Višina ${_registerData.visina}");
      print("Email ${_registerData.email}");
      print("Password ${_registerData.password}");
      dynamic result = _auth.registerWithEmailAndPassword(_registerData.ime, _registerData.priimek, _registerData.visina, _registerData.email, _registerData.password);
      if (result == null){
        print("error pri registraciji");
      } else {
        print(result.uid);
        _sendToSignInPage();
      }
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }
}

class RegisterRequestData {
  String ime = '';
  String priimek = '';
  int visina = 0;
  String email = '';
  String password = '';
}

class FormValidator {
  static FormValidator _instance;
  factory FormValidator() => _instance ??= new FormValidator._();
  FormValidator._();

  String validateIme(String value){
    String pattern = r'[A-Z][a-zA-Z]{2,20}$'; //crkke od a-z in 2-20 začetek z veliko črko
    RegExp regExp = new RegExp(pattern);
    if (value.isEmpty){
      return "Vnesite ime!";
    } else if (value.length < 2){
      return "Ime mora biti dolgo minimum 2 znaka";
    } else if (value.length > 20){
      return "Ime je lahko dolgo maksimum 20 znakov.";
    } else if (!regExp.hasMatch(value)) {
      return "Ime lahko vsebuje samo črke od A-Z in začeti se mora z veliko črko!";
    }
    return null;
  }

  String validatePriimek(String value){
    String pattern = r'[A-Z][a-zA-Z]{2,20}$'; //crkke od a-z in 2-20 začetek z veliko črko
    RegExp regExp = new RegExp(pattern);
    if (value.isEmpty){
      return "Vnesite priimek!";
    } else if (value.length < 2){
      return "Priimek mora biti dolgo minimum 2 znaka";
    } else if (value.length > 20){
      return "Priimek je lahko dolgo maksimum 20 znakov.";
    } else if (!regExp.hasMatch(value)) {
      return "Priimek lahko vsebuje samo črke od A-Z in začeti se mora z veliko črko!";
    }
    return null;
  }

  String validateVisina(String value){
    String pattern = r'[0-9]{3,3}$'; //stevilke od 0-9 in dolzina 3-3
    RegExp regExp = new RegExp(pattern);
    if (value.isEmpty){
      return "Vnesite višino!";
    } else if (value.length < 2){
      return "Višina mora biti dolgo minimum 3 številke";
    } else if (value.length > 20){
      return "Višina je lahko dolgo maksimum 3 številke.";
    } else if (!regExp.hasMatch(value)) {
      return "Višina lahko vsebuje samo črke od 0-9!";
    }
    return null;
  }

  //password validacija
  String validatePassword(String value) {
    String patttern = r'(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.isEmpty) {
      return "Vnesite geslo!";
    } else if (value.length < 8) {
      return "Geslo mora biti dolgo minimum 8 znakov!";
    } else if (!regExp.hasMatch(value)) {
      return "Geslo mora vsebovati vsan eno veliko črko, eno malo črko in eno številko!";
    }
    return null;
  }

  //email validacija
  String validateEmail(String value) {
    String pattern = r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
    RegExp regExp = new RegExp(pattern);
    if (value.isEmpty) {
      return "Vnesite email naslov!";
    } else if (!regExp.hasMatch(value)) {
      return "Neveljaven email naslov!";
    } else {
      return null;
    }
  }
}
