import 'package:flutter/material.dart';
import 'package:Seeker/screens/authenticate/register.dart';
import 'package:Seeker/screens/wrapper.dart';
import 'package:Seeker/services/auth.dart';
import 'package:Seeker/shared/loading.dart';

class SignIn extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _SignInState();
  }
}

class _SignInState extends State<SignIn> {
  final AuthService _auth = AuthService();
  GlobalKey<FormState> _key = new GlobalKey(); // key ki tracka form state
  bool _validate = false; // bool ce hocemo da se autovalidata form
  LoginRequestData _loginData =
      LoginRequestData(); // class za variabli za login
  bool skrijGeslo = true; // bool da skrijemo password text

  String error = '';
  bool loading = false;

  //statefull cidget pri setState() refresha samo kar je v buildo(layout)
  //nebo naprimer na novo instanciralo variablov
  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            // basic element z katerim se vsak app zacne
            body: Center(
              // v  body se dajaejo "widgeti", center centrira vertikalno
              child: SingleChildScrollView(
                // child centra omogoča scrolanje
                child: Container(
                  // child centra, nested
                  margin: EdgeInsets.all(20.0),
                  child: Center(
                    child: Form(
                      key: _key,
                      autovalidate: _validate,
                      child: _getFormUI(),
                    ),
                  ),
                ),
              ),
            ),
          );
  }

  Widget _getFormUI() {
    return new Column(
      // returna column v katerega lahko damo vec elementov
      children: <Widget>[
        // razporejeni bojo v "rowe"
        new Image.asset(
          // nalozimo sliko iz folderja
          'assets/logo.png', // asset folder je declaran v pubspec.yaml
          height: 150,
          width: 300,
        ),
        new SizedBox(height: 50.0), // empty box(space)
        new TextFormField(
          //text box za email form uporabljamo za form validatorje
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: InputDecoration(
            hintText: 'Email',
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          validator: FormValidator().validateEmail, //validator
          onSaved: (String value) {
            _loginData.email = value;
          },
        ),
        new SizedBox(height: 20.0),
        new TextFormField(
            autofocus: false,
            obscureText: skrijGeslo,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: 'Geslo',
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
              suffixIcon: GestureDetector(
                //gesture detection kjer lahko detectamo tape
                onTap: () {
                  setState(() {
                    skrijGeslo = !skrijGeslo; //nastavi false na true in obratno
                  });
                },
                child: Icon(
                  skrijGeslo
                      ? Icons.visibility
                      : Icons.visibility_off,
                       // statement ? true : false
                  semanticLabel: skrijGeslo ? 'prikaži geslo' : 'skrij geslo',
                ),
              ),
            ),
            validator: FormValidator().validatePassword,
            onSaved: (String value) {
              _loginData.password =
                  value; // on saved se zgodi ko klicemo _formKey.currentState.save()
            }),
        new SizedBox(height: 15.0),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: _sendToServer,
            padding: EdgeInsets.all(12),
            color: Colors.blue,
            child: Text('Prijava', style: TextStyle(color: Colors.white)),
          ),
        ),
        new Text(
          error,
          style: TextStyle(color: Colors.red, fontSize: 14.0),
        ),
        new Container(
            child: RaisedButton(
                child: Text('Sign in anon'),
                onPressed: () async {
                  dynamic result = await _auth.signInAnon();
                  if (result == null) {
                    print('error signing in');
                  } else {
                    print('signed in');
                    print(result.uid);
                    print(result.toString());
                  }
                })),
        new FlatButton(
          child: Text(
            'Izgubljeno geslo?',
            style: TextStyle(color: Colors.black54),
          ),
          onPressed: _showForgotPasswordDialog, // klicana funkcija
        ),
        new FlatButton(
          onPressed: _sendToRegisterPage,
          child: Text('Nimate računa? Registracija!',
              style: TextStyle(color: Colors.black54)),
        ),
      ],
    );
  }

  _sendToRegisterPage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Register()),
    );
  }

  _sendToHome() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Wrapper()),
    );
  }

  _sendToServer() async {
    if (_key.currentState.validate()) {
      setState(() => loading = true);
      _key.currentState.save(); // sprozi onSaved pri email pa passwordu
      print("Email ${_loginData.email}");
      print("Password ${_loginData.password}");
      dynamic result = await _auth.signInWithEmailAndPassword(
          _loginData.email, _loginData.password);
      if (result == null) {
        setState(() {
          error = 'Prijava ni uspela!';
          loading = false;
        });
      } else {
        loading = false;
        setState(() => error = '');
        _sendToHome();
      }
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  Future<Null> _showForgotPasswordDialog() async {
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: const Text('Vnesite email naslov!'),
            contentPadding: EdgeInsets.all(5.0),
            content: new TextField(
              decoration: new InputDecoration(hintText: "Email"),
              onChanged: (String value) {
                _loginData.email = value;
              },
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Pošlji"),
                onPressed: () async {
                  _auth.resetPassword(_loginData.email);
                  Navigator.pop(context);
                },
              ),
              new FlatButton(
                child: new Text("Prekliči"),
                onPressed: () => Navigator.pop(context),
              ),
            ],
          );
        });
  }
}

class LoginRequestData {
  String email = '';
  String password = '';
}

class FormValidator {
  static FormValidator _instance;
  factory FormValidator() => _instance ??= new FormValidator._();
  FormValidator._();

  //password validacija
  String validatePassword(String value) {
    String patttern = r'(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.isEmpty) {
      return "Vnesite geslo!";
    } else if (value.length < 8) {
      return "Geslo mora biti dolgo minimum 8 znakov!";
    } else if (!regExp.hasMatch(value)) {
      return "Geslo mora vsebovati vsan eno veliko črko, eno malo črko in eno številko!";
    }
    return null;
  }

  //email validacija
  String validateEmail(String value) {
    String pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
    RegExp regExp = new RegExp(pattern);
    if (value.isEmpty) {
      return "Vnesite email naslov!";
    } else if (!regExp.hasMatch(value)) {
      return "Neveljaven email naslov!";
    } else {
      return null;
    }
  }
}
