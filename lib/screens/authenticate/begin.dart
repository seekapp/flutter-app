import 'package:flutter/material.dart';
import 'package:Seeker/screens/authenticate/register.dart';
import 'package:Seeker/screens/authenticate/sign_in.dart';

class Begin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Column(
          children: [
            SizedBox(height: 130),
            Center(
              child: Image.asset(
                'assets/logo.png',
                height: 150,
                width: 300, 
              ),
            ),
            SizedBox(height: 150),
            ButtonTheme(
              minWidth: 200,
              height: 50,
              child: RaisedButton(
                child: Text(
                  'Prijava',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                onPressed: () {
                   navigateToLoginScreen(context);
                  },
              ),
            ),
            SizedBox(height: 10),
            ButtonTheme(
                minWidth: 200,
                height: 50,
                child: (RaisedButton(
                  child: Text(
                    'Registracija',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                  onPressed: () {
                    navigateToRegisterScreen(context);
                  },
                ))),
            SizedBox(
              height: 70,
            ),
            FlatButton(
              child: Text('Izhod'),
              onPressed: () {},
            )
          ],
        ),
        backgroundColor: Colors.white,
      ),
    );
  }
}

void navigateToLoginScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignIn()),
    );
}

void navigateToRegisterScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Register()),
    );
}