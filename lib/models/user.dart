class User {

  final String uid;
  User({ this.uid });
}

class UserData {
  final String uid;
  final String name;
  final String surname;
  final int height;

  UserData({this.uid, this.name, this. surname, this.height});
}