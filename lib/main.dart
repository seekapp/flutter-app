import 'package:flutter/material.dart';
import 'package:Seeker/screens/wrapper.dart';
import 'package:provider/provider.dart';
import 'package:Seeker/services/auth.dart';
import 'package:Seeker/models/user.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //wrapamo root widget z stream provider v wrapper lahko accesamo streamprovider<user>.value (listenamo na value)
    return StreamProvider<User>.value(
           value: AuthService().user,
          child: MaterialApp(
        home: Wrapper(),
      ),
    );
  }
}